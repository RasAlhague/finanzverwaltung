DROP TABLE IF EXISTS transaction_types;

CREATE TABLE IF NOT EXISTS transaction_types (
    id SERIAL PRIMARY KEY,
    type_name VARCHAR (10) NOT NULL
);