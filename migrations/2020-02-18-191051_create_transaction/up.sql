CREATE TABLE IF NOT EXISTS transactions (
    id SERIAL PRIMARY KEY,
    transaction_name VARCHAR (255) NOT NULL,
    origin VARCHAR (255) NOT NULL,
    reason VARCHAR (255),
    create_date TIMESTAMP NOT NULL DEFAULT NOW(),
    effective_date TIMESTAMP NOT NULL,
    amount DOUBLE PRECISION NOT NULL,
    type_id INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS transaction_types (
    id SERIAL PRIMARY KEY,
    type_name VARCHAR (10) NOT NULL
);

CREATE TABLE IF NOT EXISTS standing_orders (
    id SERIAL PRIMARY KEY,
    order_name VARCHAR (255) NOT NULL,
    start_date TIMESTAMP NOT NULL,
    end_date TIMESTAMP,
    create_date TIMESTAMP NOT NULL DEFAULT NOW(),
    transaction_id INTEGER NOT NULL
);