# Finanzverwaltungssystem

## Beschreibung

Programm zum Verwalten der aktuellen Finanzen. Soll monatliche Einnahmen und Ausgaben überwachen.
Einnahmen können als fortlaufend definiert werden, ausgaben ebenfalls. Es soll eine monatliche übersicht geben.
Einnahmen sollen einen Namen eine Herkunft und einen Grund haben, sowie den Betrag. Ausgaben sollen einen Namen
eine Herkunft einen Grund, sowie den betrag haben. 

## Umsetzung

 - rust oder c#
 - Web api als schnittstelle
 - mysql db
 - frontend entweder c# client oder rust client
 - idee rust web api, c# client
 - basierend auf xamarin
 - oder einfach eine website dann noch davor böllern
 - app fände ich schöner

## Web Api

### expense_standing_order
| Method | Path 
| --- | --- 
| GET | /api/**transaction**/**standing_order**
| GET | /api/**transaction**/**standing_order**/{filter}
| GET | /api/**transaction**/**standing_order**/{id} 
| PUT | /api/**transaction**/**standing_order**/{id} 
| POST | /api/**transaction**/**standing_order** 
| DELTE | /api/**transaction**/**standing_order**/{id} 

### expense
| Method | Path 
| --- | --- 
| GET | /api/**transaction**
| GET | /api/**transaction**/{filter}
| GET | /api/**transaction**/{id} 
| PUT | /api/**transaction**/{id} 
| POST | /api/**transaction** 
| DELETE | /api/**transaction**/{id} 

## DB Struktur

### standing_order
| name | typ | details | examples 
|---|---|---|---
| id | int | PK AI | 1 
| start_date | datetime | NOT NULL | 27.02.2020 
| name | varchar(255) | NOT NULL | "Dauer gehalt"
| end_date | datetime | empty means no enddate | 
| transaction_id | int | FK transaction(id) | 1 

### transaction
| name | typ | details | examples 
|---|---|---|---
| id | int | PK AI | 1 
| name | varchar(255) | NOT NULL | "UTS Gehalt" 
| origin | varchar(255) | NOT NULL | "Gehalt von der Arbeit" 
| reason | varchar(255) | no reason required | 
| effective_date | datetime | NOT NULL | 27.02.2020 08:30 
| create_date | datetime | NOT NULL | 13.02.2020 08:32 
| amount | double | NOT NULL | 560.60 
| type_id | int | FK transaction_type(id) NOT NULL | 1 

### transaction_type 
| name | typ | details | examples 
|---|---|---|---
| id | int | PK AI | 1 
| name | varchar(7) | NOT NULL | "income"

## SQL:

```sql
-- Drop table statements

DROP TABLE IF EXISTS standing_order;
DROP TABLE IF EXISTS transaction;
DROP TABLE IF EXISTS transaction_type;

-- Create table statements

CREATE TABLE IF NOT EXISTS standing_order (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    start_date TIMESTAMP NOT NULL,
    end_date TIMESTAMP,
    transaction_id INTEGER,
    FOREIGN KEY transaction_id REFERENCES transaction(id)
);

CREATE TABLE IF NOT EXISTS transaction (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    origin VARCHAR(255) NOT NULL,
    reason VARCHAR(255),
    effective_date TIMESTAMP NOT NULL,
    create_date TIMESTAMP NOT NULL,
    amount DOUBLE NOT NULL,
    type_id INTEGER NOT NULL,
    FOREIGN KEY type_id REFERENCES transaction_type(id)
);

CREATE TABLE IF NOT EXISTS transaction_type (
    id SERIAL PRIMARY KEY,
    name VARCHAR(7) NOT NULL
);

-- Insert statements für transaction_type

INSERT INTO transaction_type (name) 
VALUES ("income"),
       ("expense");

-- Sample insert statements

INSERT INTO transaction (name, origin, reason, effective_date, create_date, amount, type_id)
VALUES ("Gehalt", "UTS GmbH", "Arbeiten", NOW(), NOW(), 560.50, 1),
       ("Eltern Geld", "Ralf Müller", "Teilübenahme kosten", NOW(), NOW(), 100.0, 2),
       ("Chipotle Crunch Chicken", "Burrito Bay area", "Mittagessen", NOW(), NOW(), 7.75, 2),
       ("Dead Cells", "Steam", NULL, NOW(), NOW(), 20.23, 2);

INSERT INTO standing_order (start_date, end_date, name, transaction_id) 
VALUES (NOW(), NOW(), "Dauerauftrag Gehalt", 1),
       (NOW(), NOW(), "Dauerauftrag Elterngeld", 2);

SELECT * 
  FROM transaction AS t, standing_order AS so 
 WHERE t.id = so.transaction_id

-- Crud for transaction

INSERT INTO transaction (name, origin, reason, effective_date, create_date, amount, type_id)
VALUES (:name, :origin, :reason, :effective_date, NOW(), :amount, :type_id),

-- Standard
SELECT id, origin, reason, effective_date, create_date, amount, type_id
  FROM transaction;

-- filtering by id
SELECT id, origin, reason, effective_date, create_date, amount, type_id
  FROM transaction

UPDATE transaction 
   SET name = :name,
       origin = :origin,
       reason = :reason,
       effective_date = :effective_date,
       create_date = :create_date,
       amount = :amount,
       type_id = :type_id
 WHERE id = :id;

DELETE FROM transaction
 WHERE id = :id;

-- Crud for standing_order

INSERT INTO standing_order(start_date, end_date, name, transaction_id)
VALUES (:start_date, :end_date, :name, :transaction_id);

SELECT id, start_date, end_date, name, transaction_id
  FROM standing_order;

SELECT id, start_date, end_date, name, transaction_id
  FROM standing_order
 WHERE id = :id;

UPDATE standing_order
   SET start_date = :start_date,
       end_date = :end_date,
       name = :name,
       transaction_id = :transaction_id
 WHERE id = :id;

DELETE FROM standing_order 
 WHERE id = :id;

```

```rs
trait DbModel {
    fn create(model: Self, conn: &MySqlConn) -> Result<Self, &'static str>;
    fn read(id: u32, conn: &MySqlConn) -> Result<Option<Self>, &'static str>;
    fn read_filtered(id: u32, conn: &MySqlConn, filter: QueryFilter) -> Result<Vec<Self>, &'static str>;
    fn update_from(model: Self, conn: &MySqlConn) -> Result<Self, &'static str>;
    fn delete(id: u32, conn: &MySqlConn) -> Result<Option<()>, &'static str>;
}

struct Transaction {
  
}
```