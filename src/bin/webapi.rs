#![feature(proc_macro_hygiene, decl_macro)]

extern crate diesel;
extern crate finanzverwaltung;

use self::finanzverwaltung::*;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;

fn rocket() -> rocket::Rocket {
    rocket::ignite().mount(
        "/api",
        routes![
            self::api::standing_order::get_all,
            self::api::standing_order::get,
            self::api::standing_order::put,
            self::api::standing_order::post,
            self::api::standing_order::delete,
            self::api::transaction::get_all,
            self::api::transaction::get,
            self::api::transaction::put,
            self::api::transaction::post,
            self::api::transaction::delete
        ],
    )
}

fn main() {
    rocket().launch();
}
