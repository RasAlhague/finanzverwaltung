table! {
    standing_orders (id) {
        id -> Int4,
        order_name -> Varchar,
        start_date -> Timestamp,
        end_date -> Nullable<Timestamp>,
        create_date -> Timestamp,
        transaction_id -> Int4,
    }
}

table! {
    transactions (id) {
        id -> Int4,
        transaction_name -> Varchar,
        origin -> Varchar,
        reason -> Nullable<Varchar>,
        create_date -> Timestamp,
        effective_date -> Timestamp,
        amount -> Float8,
        type_id -> Int4,
    }
}

table! {
    transaction_types (id) {
        id -> Int4,
        type_name -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(standing_orders, transactions, transaction_types,);
