pub mod transaction {
    use crate::models::transactions::{NewTransaction, Transaction};
    use crate::*;
    use diesel::prelude::*;
    use rocket::http::Status;
    use rocket_contrib::json::Json;

    #[get("/transaction")]
    pub fn get_all() -> Json<Vec<Transaction>> {
        use crate::schema::transactions::dsl::*;

        let conn = establish_connection();

        let results = transactions
            .load::<Transaction>(&conn)
            .expect("Error loading transactions");

        Json(results)
    }

    #[get("/transaction/<id_t>")]
    pub fn get(id_t: i32) -> Json<Transaction> {
        use crate::schema::transactions::dsl::*;

        let conn = establish_connection();

        let result: Transaction = transactions
            .find(id_t)
            .get_result(&conn)
            .expect("Error loading transaction");

        Json(result)
    }

    #[put(
        "/transaction/<id>",
        format = "application/json",
        data = "<transaction>"
    )]
    pub fn put(id: i32, transaction: Json<Transaction>) -> Json<Transaction> {
        use crate::schema::transactions::dsl::{
            amount, create_date, effective_date, origin, reason, transaction_name, transactions,
            type_id,
        };

        let conn = crate::establish_connection();

        let t: Transaction = diesel::update(transactions.find(id))
            .set((
                transaction_name.eq(transaction.transaction_name.clone()),
                origin.eq(transaction.origin.clone()),
                amount.eq(transaction.amount),
                create_date.eq(transaction.create_date),
                effective_date.eq(transaction.effective_date),
                reason.eq(transaction.reason.clone()),
                type_id.eq(transaction.type_id),
            ))
            .get_result(&conn)
            .expect("Error saving new transaction");

        Json(t)
    }

    #[post("/transaction", format = "application/json", data = "<transaction>")]
    pub fn post(transaction: Json<Transaction>) -> Json<i32> {
        use crate::schema::transactions;

        let conn = crate::establish_connection();

        let new_transaction = NewTransaction {
            transaction_name: transaction.transaction_name.clone(),
            origin: transaction.origin.clone(),
            reason: transaction.reason.clone(),
            create_date: transaction.create_date,
            effective_date: transaction.effective_date,
            amount: transaction.amount,
            type_id: transaction.type_id,
        };

        let t: Transaction = diesel::insert_into(transactions::table)
            .values(&new_transaction)
            .get_result(&conn)
            .expect("Error saving new transaction");

        Json(t.id)
    }

    #[delete("/transaction/<id_t>")]
    pub fn delete(id_t: i32) -> Status {
        use crate::schema::transactions::dsl::*;

        let conn = establish_connection();

        diesel::delete(transactions.filter(id.eq(id_t)))
            .execute(&conn)
            .expect("Error deleting posts");

        Status::Accepted
    }
}

pub mod standing_order {
    use crate::models::standing_orders::StandingOrder;
    use crate::*;
    use diesel::prelude::*;
    use rocket::http::Status;
    use rocket_contrib::json::Json;

    #[get("/standing_order")]
    pub fn get_all() -> Json<Vec<StandingOrder>> {
        use crate::schema::standing_orders::dsl::*;

        let conn = establish_connection();

        let results = standing_orders
            .load::<StandingOrder>(&conn)
            .expect("Error loading standinf_orders");

        Json(results)
    }

    #[get("/standing_order/<id_s>")]
    pub fn get(id_s: i32) -> Json<StandingOrder> {
        use crate::schema::standing_orders::dsl::*;

        let conn = establish_connection();

        let result: StandingOrder = standing_orders
            .find(id_s)
            .get_result(&conn)
            .expect("Error loading standing_order");

        Json(result)
    }

    #[put(
        "/standing_order/<id>",
        format = "application/json",
        data = "<standing_order>"
    )]
    pub fn put(id: i32, standing_order: Json<StandingOrder>) -> Json<StandingOrder> {
        use crate::schema::standing_orders::dsl::{
            create_date, end_date, order_name, standing_orders, start_date, transaction_id,
        };

        let conn = crate::establish_connection();

        let t: StandingOrder = diesel::update(standing_orders.find(id))
            .set((
                order_name.eq(standing_order.order_name.clone()),
                create_date.eq(standing_order.create_date.clone()),
                start_date.eq(standing_order.start_date),
                end_date.eq(standing_order.end_date),
                transaction_id.eq(standing_order.transaction_id),
            ))
            .get_result(&conn)
            .expect("Error saving new transaction");

        Json(t)
    }

    #[post(
        "/standing_order",
        format = "application/json",
        data = "<standing_order>"
    )]
    pub fn post(standing_order: Json<StandingOrder>) -> Json<i32> {
        use crate::models::standing_orders::{NewStandingOrder, StandingOrder};
        use crate::schema::standing_orders;

        let conn = crate::establish_connection();

        let new_standing_order = NewStandingOrder {
            order_name: standing_order.order_name.clone(),
            create_date: standing_order.create_date,
            start_date: standing_order.start_date,
            end_date: standing_order.end_date,
            transaction_id: standing_order.transaction_id,
        };

        let t: StandingOrder = diesel::insert_into(standing_orders::table)
            .values(&new_standing_order)
            .get_result(&conn)
            .expect("Error saving new transaction");

        Json(t.id)
    }

    #[delete("/standing_order/<id_t>")]
    pub fn delete(id_t: i32) -> Status {
        use crate::schema::standing_orders::dsl::*;

        let conn = establish_connection();

        diesel::delete(standing_orders.filter(id.eq(id_t)))
            .execute(&conn)
            .expect("Error deleting posts");

        Status::Accepted
    }
}
