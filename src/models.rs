pub mod transactions {
    use crate::schema::transactions;
    use chrono::NaiveDateTime;
    use serde::{Deserialize, Serialize};

    #[derive(Queryable, Serialize, Deserialize)]
    pub struct Transaction {
        pub id: i32,
        pub transaction_name: String,
        pub origin: String,
        pub reason: Option<String>,
        pub create_date: NaiveDateTime,
        pub effective_date: NaiveDateTime,
        pub amount: f64,
        pub type_id: i32,
    }

    #[derive(Insertable)]
    #[table_name = "transactions"]
    pub struct NewTransaction {
        pub transaction_name: String,
        pub origin: String,
        pub reason: Option<String>,
        pub create_date: NaiveDateTime,
        pub effective_date: NaiveDateTime,
        pub amount: f64,
        pub type_id: i32,
    }

    impl Transaction {
        pub fn empty() -> Transaction {
            Transaction {
                id: 0,
                transaction_name: String::new(),
                origin: String::new(),
                reason: None,
                create_date: NaiveDateTime::from_timestamp(0, 42_000_000),
                effective_date: NaiveDateTime::from_timestamp(0, 42_000_000),
                amount: 0.0,
                type_id: 0,
            }
        }
    }

    #[derive(Queryable)]
    pub struct TransactionType {
        pub id: i32,
        pub type_name: String,
    }

    impl TransactionType {
        pub fn empty() -> TransactionType {
            TransactionType {
                id: 0,
                type_name: String::new(),
            }
        }
    }
}

pub mod standing_orders {
    use crate::schema::standing_orders;
    use chrono::NaiveDateTime;
    use serde::{Deserialize, Serialize};

    #[derive(Queryable, Serialize, Deserialize)]
    pub struct StandingOrder {
        pub id: i32,
        pub order_name: String,
        pub start_date: NaiveDateTime,
        pub end_date: Option<NaiveDateTime>,
        pub create_date: NaiveDateTime,
        pub transaction_id: i32,
    }

    #[derive(Insertable)]
    #[table_name = "standing_orders"]
    pub struct NewStandingOrder {
        pub order_name: String,
        pub start_date: NaiveDateTime,
        pub end_date: Option<NaiveDateTime>,
        pub create_date: NaiveDateTime,
        pub transaction_id: i32,
    }

    impl StandingOrder {
        pub fn empty() -> StandingOrder {
            StandingOrder {
                id: 0,
                order_name: String::new(),
                start_date: NaiveDateTime::from_timestamp(0, 42_000_000),
                end_date: None,
                create_date: NaiveDateTime::from_timestamp(0, 42_000_000),
                transaction_id: 0,
            }
        }
    }
}
